#!/bin/bash
export LD_LIBRARY_PATH=./lib
name_viking=SLICE-RTMP
while true; do 
	ProcNumber=`ps -ef |grep -w $name_viking|grep -v grep|wc -l`
	if [ $ProcNumber -le 0 ];then 
		echo "${name_viking} is not running"
		nohup ./${name_viking}>/dev/null 2>&1 &
	else
		echo "${name_viking}'s pid is ${pid}"
	fi
	sleep 10
done

