#include "HttpServer.h"
#include "hssl.h"
#include <memory>
#include <mutex>
#include <thread>
#include <functional>
extern "C" {
#include <libavformat/avformat.h>
#include <libavutil/time.h>
}

struct param_t {
    char operation[20] = {0};
    char new_pad_url[512]={0};
    char old_pad_url[512]={0};
};

class HttpHandler {
public:
    HttpHandler(std::string padPath,std::string pushAddress);

    ~HttpHandler();
    void init();

    static void httpHandleThead(std::function<std::string(std::string operation,std::string newpadUrl)> delTask,void* operation,void* padUrl,bool& isPlay,void* mtx);
    static void pushPadStream(void* pHttpHandler);
    std::string handleFunc(std::string operation,std::string newpadUrl);
    int initOutContent();
    int initInContent();
    std::string getPadUrl();
    bool getPlay();

    int64_t m_lastOuttime;
    int64_t m_lastIntime;
    int64_t m_timeout;
private:
    int m_port;
    std::string m_padUrl;
    bool m_isPlay;
    std::string m_pushAddress;
    std::string m_operation;
    std::thread m_serverThread;
    std::thread m_pushPadStream;
    std::mutex m_mtx;
    
    std::shared_ptr<AVFormatContext> m_pInCtx;
    std::shared_ptr<AVFormatContext> m_pOutCtx;
    std::function<std::string(std::string operation,std::string newpadUrl)> m_handleFunc;
};
