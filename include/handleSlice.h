#include <iostream>
#include <string>
#include <queue>

class SliceHandler{
private:
    std::string m_strPath;
    long int* m_strDate;
    std::string *m_strHourSliceDir;

public:
    SliceHandler(std::string slicePath,time_t& date,std::string& hourSliceDir);
    int readShiftNsi(std::string fileName,std::queue<std::string>& qSlice);
    void sliceProducer(std::queue<std::string>& qSlices);
    ~SliceHandler();
};