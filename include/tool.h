#include <iostream>
#include <ctime>
#include <string>
#include <dirent.h>
#include <string.h>
#include <queue>
#include <fstream>
#include <sstream>

static time_t getCurrentTime() {
    time_t now = time(NULL);
    return now;
}

// 基于当前系统的当前日期/时间
static std::string getCurrentTimeHour() {
    time_t now = time(0);

    tm *ltm = localtime(&now);

    char temp[32] ;
    std::string time ="";
    sprintf(temp,"%d",ltm->tm_year) ;
    time += temp;
    sprintf(temp,"%d",ltm->tm_mon) ;
    time += temp;
    sprintf(temp,"%d",ltm->tm_mday) ;
    time += temp;
    sprintf(temp,"%d",ltm->tm_hour) ;
    time += temp;

    return time;
}

static bool isNum(std::string str)
{
    std::stringstream sin(str);
    double d;
    char c;
    if(!(sin >> d))
        return false;
    if (sin >> c)
        return false;
    return true;
}

static int getBeiJinTimeInSecs(std::string strTime,tm& res){
    int index = strTime.find_last_of('-');
    if (index == -1) {
        printf("wrong date construct:%s",strTime.c_str());
        return -1;
    }

    int date[6] = {0};
    auto temp1 = strTime;
    auto temp2 = strTime;
    auto num = 0;
    while(index != -1){
        temp1 = temp2.substr(index+1);
        if(!isNum(temp1)) {
            printf("wrong date construct:%s",strTime.c_str());
            return -1;
        }
        temp2 =temp2.substr(0,index);
        int str = atoi(temp1.c_str());

        index = temp2.find_last_of('-');
        date[num] = str;
        num++;
        if(index == -1) {
            //year
            date[num] = atoi(temp2.c_str());
            num++;
        }

        if(num >= 6) {
            break;
        }
    }
    
    res.tm_sec = date[0];
    res.tm_min = date[1];			
    res.tm_hour = date[2];
    res.tm_mday = date[3];
    res.tm_mon = date[4] - 1;
    res.tm_year = date[5] - 1900;
    res.tm_isdst = -1;
    return 0;
}

static std::string getSliceHourDirByInput(std::string date) {
    std::string res;
    for (int i = 0 ; i < 4; i++) {
        int index = date.find_first_of('-');
        if (index == -1) {
            printf("wrong date construct:%s",date.c_str());
            return "";
        }
        res += date.substr(0,index);
        date = date.substr(index+1);
    }
    
    return res;
}

static std::string getSliceHourDirBySeconds(tm temp) {
    int tm_year = temp.tm_year + 1900;
    int tm_month = temp.tm_mon + 1;
    int tm_mday = temp.tm_mday;
    int tm_hour  = temp.tm_hour;
    std::string year = std::to_string(tm_year);
    if(year.size() < 2) {
        year = "0" + year;
    }
    std::string month = std::to_string(tm_month);
    if(month.size() < 2) {
        month = "0" + month;
    }
    std::string day = std::to_string(tm_mday);
    if(day.size() < 2) {
        day = "0" + day;
    }
    std::string hour = std::to_string(tm_hour);
    if(hour.size() < 2) {
        hour = "0" + hour;
    }

    return year + month + day + hour;
 }

static int readShiftNsi(std::string fileName,std::queue<std::string>& qSlice) {
    auto path = fileName + "/shift.nsi";
    std::ifstream f();
    FILE * fp;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
	fp = fopen(path.c_str(), "r");

    char csvLine[100]={0};//重要！！！ 100 一般设为一行的最大长度
    std::string sliceName;
    while((read = getline(&line, &len, fp)) != -1)
    {
        std::string word(line);
        int index = word.find_first_of('\t');
        if(-1 == index) {
            continue;
        }
        sliceName = word.substr(0,index);
        qSlice.push(fileName+ "/" + sliceName);
        //FIX:slice update
    }
    fclose(fp);
}

//遍历文件夹
static int listDir(const char *path,std::queue<std::string>& qSlice) 
{ 
    DIR              *pDir ; 
    struct dirent    *ent  ; 
    char              childpath[512]; 

    pDir=opendir(path); 
    if(NULL == pDir) {
        printf("Open dir Failed : %s",path);
        return -1;
    }
    memset(childpath,0,sizeof(childpath)); 

    while((ent=readdir(pDir))!=NULL) 
    { 

        if(ent->d_type & DT_DIR) 
        { 

                if(strcmp(ent->d_name,".")==0 || strcmp(ent->d_name,"..")==0) 
                        continue; 

                sprintf(childpath,"%s/%s",path,ent->d_name); 
                readShiftNsi(childpath,qSlice); 
        } 
        // else
        // {
        //     printf("ent->d_name = %s",ent->d_name);
        // }
    }

    return 0;
}

static std::string GetEnv(std::string envName) {
    getenv(envName.c_str());
}
