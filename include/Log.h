
#ifndef LOG_H_
#define LOG_H_
#include <unistd.h>
#include <mutex>

#include "log4cpp/Category.hh"
#include "log4cpp/OstreamAppender.hh"
#include "log4cpp/BasicLayout.hh"
#include "log4cpp/Priority.hh"
#include "log4cpp/PropertyConfigurator.hh"
#include "log4cpp/RollingFileAppender.hh"
#include "log4cpp/PatternLayout.hh"
#include "log4cpp/DailyRollingFileAppender.hh"

class Log {
public:
    static Log * GetInstance();
    bool Configure(const char* log_dir,const char* log_name,size_t max_file_size,size_t max_files);
    log4cpp::Category * Category();
    ~Log();

private:
    Log();
    Log(const Log&){};
    Log& operator=(const Log&){return *this;}
private:
    log4cpp::Category * m_log = nullptr;
    static std::mutex m_mtx;
    static Log* m_instance;
};


#define MFT_LOGER  (Log::GetInstance()->Category())

// #define SLOG_DEBUG LOG4CPP_DEBUG_S((*MFT_LOGER)) << "[PID]" << (unsigned)getpid() << "[TID]" << (unsigned)pthread_self() << " | "
// #define SLOG_ERROR LOG4CPP_ERROR_S((*MFT_LOGER)) << "[PID]" << (unsigned)getpid() << "[TID]" << (unsigned)pthread_self() << " | "
#define SLOG_DEBUG LOG4CPP_DEBUG_S((*MFT_LOGER))
#define SLOG_ERROR LOG4CPP_ERROR_S((*MFT_LOGER))


#endif 
