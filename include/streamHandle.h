#include <iostream>
#include <memory>
#include <queue>
extern "C" {
#include <libavformat/avformat.h>
#include <libavutil/time.h>
}

class StreamHandler {

public:
    StreamHandler(std::string url);
    ~StreamHandler();
    
    int init();
    void getPkts(std::string inUrl);
    int writeFrame();
    int writeFrameByUrl(std::string inUrl,std::queue<std::string>& qslice,bool& isPlaySlice);
    int getPackets();

    int64_t m_lasttime;
    int64_t m_timeout;

private:
    std::queue<AVPacket*> m_qPackets;
    std::queue<AVPacket*> m_qAPackets;
    std::shared_ptr<AVFormatContext> m_pInCtx;
    std::shared_ptr<AVFormatContext> m_pOutCtx;

    std::string m_outUrl;
    long long m_startTime;
    long long m_firstPktDts;
    std::string cur_url;
    int m_vIndex;
    int m_aIndex;

};
