#include <iostream>
#include <condition_variable>
#include <mutex>
#include <thread>
#include <unistd.h>
#include "Log.h"
#include "tool.h"
#include "streamHandle.h"
#include "handleSlice.h"
#include "httpHand.h"
#include "http_content.h"

// #include "json.h"

void threadReadPacket(void* pStreamHandler, void* pSliceHandler, void* pQSlices,void* pAddCV,void* pMtx,bool& isDisContinue,void* pHttpHandler) {
    StreamHandler* streamHandler = (StreamHandler*)(pStreamHandler);
    SliceHandler* sliceHandler = (SliceHandler*)(pSliceHandler);
    HttpHandler* httpHandler = (HttpHandler*)(pHttpHandler);
    std::queue<std::string>* pQueSlices = (std::queue<std::string>*)(pQSlices);
    std::condition_variable_any* addCV = (std::condition_variable_any*)pAddCV;
    std::mutex* mtx = (std::mutex*)pMtx;

    if(pQueSlices->size() <= 0) {
        SLOG_ERROR << "slices size is 0...";
        printf("slices size is 0.");//FIX:
    }

    while(!isDisContinue && httpHandler->getPlay()){
        std::unique_lock<std::mutex> lock(*mtx);
        addCV->wait(lock,[&]{
            int size = streamHandler->getPackets();
            if(streamHandler->getPackets() >= 10 && false == isDisContinue){
                return false;
            }else{
                return true;
            }
        });

        while (pQueSlices->size() == 0) {
            sliceHandler->sliceProducer(*pQueSlices);
            sleep(1);
        }

        while(streamHandler->getPackets() < 10 && pQueSlices->size() > 0) {
            std::string url = pQueSlices->front();
            pQueSlices->pop();
            SLOG_DEBUG << "pQueSlices size = " << pQueSlices->size();
            SLOG_DEBUG << "push slice file : " << url;
            printf("push url:%s",url.c_str());
            streamHandler->getPkts(url);//FIX:
            SLOG_DEBUG << "v packet size= " << streamHandler->getPackets();
        }

        addCV->notify_all();
        lock.unlock();
    }
}

void threadPushStream(void* pStreamHandler,void* pAddCV,void* pMtx,bool& isDisContinue,void* pHttpHandler) {
    StreamHandler* streamHandler = (StreamHandler*)(pStreamHandler);
    HttpHandler* httpHandler = (HttpHandler*)(pHttpHandler);
    std::condition_variable_any* addCV = (std::condition_variable_any*)pAddCV;
    std::mutex* mtx = (std::mutex*)pMtx;
    while(!isDisContinue && httpHandler->getPlay()){
        std::unique_lock<std::mutex> lock(*mtx);
        addCV->wait(lock,[=]{
            if(streamHandler->getPackets() == 0){
                return false;
            }else{
                return true;
            }
        });

        while(streamHandler->getPackets() >= 2){
            if(streamHandler->writeFrame() < 0) {
                isDisContinue = true;
                break;
            }
        }

        addCV->notify_all();
        lock.unlock();
    }
}

void readSlice(void* pSliceHandler, void* pQSlices,void* pMtx) {
    SliceHandler* sliceHandler = (SliceHandler*)(pSliceHandler);
    std::queue<std::string>* pQueSlices = (std::queue<std::string>*)(pQSlices);
    std::mutex* mtx = (std::mutex*)pMtx;
    while (pQueSlices->size() == 0) {
        std::unique_lock<std::mutex> lock(*mtx);
        sliceHandler->sliceProducer(*pQueSlices);
        sleep(1);
        lock.unlock();
    }
}

void playPad(void* pHttpHander,bool isPlay, void* pMtx) {
    HttpHandler* httpHander = (HttpHandler*)(pHttpHander);
    std::mutex* mtx = (std::mutex*)pMtx;
    while (1) {
        std::unique_lock<std::mutex> lock(*mtx);
        isPlay = httpHander->getPlay();
        sleep(1);
        lock.unlock();
    }
}

int CreateLogDir(const char *sPathName)
{
      char DirName[256];
      strcpy(DirName, sPathName);
      int i,len = strlen(DirName);
      for(i=1; i<len; i++)
      {
          if(DirName[i]=='/')
          {
              DirName[i] = 0;
              if(access(DirName, NULL)!=0)
              {
                  if(mkdir(DirName, 0777)==-1)
                  {
                      printf("mkdir   error\n");
                      return -1;
                  }
              }
              DirName[i] = '/';
          }
      }
      return 0;
}

#include "json.hpp"
using json = nlohmann::json;

int main() {
    // printf("Get env");
    // std::string slicePath = getenv("SLICEPATH");
    // if (slicePath.empty()){
    //     printf("Failed to get env %s!!",slicePath.c_str());
    //     return -1;
    // }

    // //ms slice flag
    // std::string msSliceFlag = getenv("MILLISECONDSSLICEFLAG");
    // if (msSliceFlag.empty()){
    //     printf("Failed to get env %s!!",msSliceFlag.c_str());
    //     return -1;
    // }

    // std::string sliceStartTime = getenv("STARTPUSHSLICTIME");
    // if (sliceStartTime.empty()){
    //     printf("Failed to get env %s!!",sliceStartTime.c_str());
    //     return -1;
    // }
    // std::string waitTime = getenv("WAITSLICETIME");
    // if (waitTime.empty()){
    //     printf("Failed to get env %s!!",waitTime.c_str());
    //     return -1;
    // }
    // std::string pushStreamAddress = getenv("PUSHSTREAMADDRESS");
    // if (pushStreamAddress.empty()){
    //     printf("Failed to get env %s!!",pushStreamAddress.c_str());
    //     return -1;
    // }
    // std::string padPath = getenv("PADPATH");
    // if (padPath.empty()){
    //     printf("Failed to get env %s!!",padPath.c_str());
    //     return -1;
    // }
    
    std::string slicePath = "/home/xxj/workspace/VIKING-slice2rtmp/h264_8000k_flv/";
    std::string msSliceFlag = "0";
    std::string sliceStartTime = "2022-05-16-10-00-00";
    std::string waitTime = "10";
    std::string pushStreamAddress = "rtmp://172.29.3.23/live/livestream2";
    std::string padPath = "http://liveyc.cmc.yuechirmt.cn/live/yctv.flv";

    int max_file_size = 10*1024*1024;
    int max_files = 7;
    auto logger = Log::GetInstance();
	logger->Configure("./Log/","vikingslice2rtmp.log",max_file_size,max_files);

    // httpHandle();
    bool isPlaySlice = true; //true: normal ,false: play pad stream 
    HttpHandler httpHandler(padPath,pushStreamAddress);

    tm time;
    getBeiJinTimeInSecs(sliceStartTime,time);
    time_t threadStartTime = mktime(&time) + std::atoi(waitTime.c_str());
    
    std::string sliceHourDir = "";//getSliceHourDirByInput(sliceStartTime)
    bool isDisContinue = false;
    int delayCount = 1; //slice file delay bug

    while (1) {
        if(false == httpHandler.getPlay()) {
            SLOG_ERROR << "play pad stream = " << httpHandler.getPadUrl();
            continue;
        } 
        if(isDisContinue) {
            isDisContinue = false;
            break;
        }
        time_t curTime = getCurrentTime();
        time_t tempSliceTime = mktime(&time) + std::atoi(waitTime.c_str());

        if(curTime > tempSliceTime) {
            tempSliceTime = curTime - std::atoi(waitTime.c_str()) * delayCount;
            printf("delaycount = %d\n",delayCount);
            SLOG_ERROR << "delaycount = " << delayCount;
        }

        //更新小时分片文件夹
        time_t seconds = tempSliceTime - 8*60*60;
        tm *temp = localtime(&seconds);
        sliceHourDir = getSliceHourDirBySeconds(*temp);
        
        //只推（当前时间 - 等待时间）之后的分片
        time_t sliceTime = std::atoi(msSliceFlag.c_str()) <= 0 ? (tempSliceTime): (tempSliceTime)*1000;

        // TEST
        // sliceTime = 1653014100;
        // sliceHourDir = "2022051602";

        if(curTime < threadStartTime) {
            printf("wait...threadStartTime = %ld\n",threadStartTime);
            SLOG_DEBUG << "waiting..." << "threadStartTime = " << threadStartTime;
            sleep(1);
            continue;
        }

        SLOG_DEBUG << "start !!";
        std::mutex mtx;
        std::condition_variable_any conVar;

        std::queue<std::string> qslice;
        //提前处理分片
        SliceHandler handler(slicePath,sliceTime,sliceHourDir);
        handler.sliceProducer(qslice);
        if(qslice.empty()){
            printf("wait reconnect!!\n");
            SLOG_ERROR << "wait reconnect!!";
            delayCount += 1;

            //垫片
            sleep(10);
            std::thread slicer(readSlice,(void*)&handler,(void*)&qslice,(void*)&mtx);
            pthread_setname_np(slicer.native_handle(),"slicer");
            std::thread padhandler(playPad,(void*)&httpHandler,std::ref(isPlaySlice),(void*)&mtx);
            pthread_setname_np(padhandler.native_handle(),"padhandler");

            StreamHandler padStreamHandler(pushStreamAddress);
            padStreamHandler.writeFrameByUrl(padPath,qslice,isPlaySlice);//httpHandler.getPicUrl()

            slicer.join();
            padhandler.join();
            continue;
        }

        delayCount = 1;

        StreamHandler streamHandler(pushStreamAddress);

        //init
        std::string url = qslice.front();
        qslice.pop();
        streamHandler.getPkts(url);
        int ret = streamHandler.init();
        if(ret < 0) {
            sleep(60);//wait out url
            continue;
        }

        std::thread producer(threadReadPacket,(void*)&streamHandler,(void*)&handler,(void*)&qslice,(void*)&conVar,(void*)&mtx,std::ref(isDisContinue),(void*)&httpHandler);
        pthread_setname_np(producer.native_handle(),"producer");
        std::thread consumer(threadPushStream,(void*)&streamHandler,(void*)&conVar,(void*)&mtx,std::ref(isDisContinue),(void*)&httpHandler);
        pthread_setname_np(consumer.native_handle(),"consumer");
        //FIX: stop stream
        producer.join();
        consumer.join();
        SLOG_DEBUG << "finish !!";
    }

    return 0;
}