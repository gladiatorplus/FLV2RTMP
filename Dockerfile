FROM registry.cn-beijing.aliyuncs.com/cmc/sm:VikingBuilder-beta-4-g0c21f78 as builder

# RUN  yum install gcc gcc-c++ make cmake -y

ADD . /root/src/SLICE2RTMP/

RUN mkdir -p /root/src/build &&\
    cd /root/src/build &&\
    cmake -DCMAKE_BUILD_TYPE=Debug \
    ../SLICE2RTMP &&\
    make -j$(nproc) 

FROM registry.cn-beijing.aliyuncs.com/cmc/centos:1.14

# 公司的基础镜像适合在 aliyun 的机器上 
# 但如果不是 aliyun 的机器, 需要使用下面这个 repo 
RUN  wget -O /etc/yum.repos.d/CentOS-Base.repo https://mirrors.aliyun.com/repo/Centos-7.repo

RUN yum install -y gdb

ADD lib /usr/mcloud/VIKING-SLICE2RTMP/lib
ADD include /usr/mcloud/VIKING-SLICE2RTMP/include
# ADD h264_8000k_flv /usr/mcloud/VIKING-SLICE2RTMP/h264_8000k_flv
ADD run.sh /usr/mcloud/VIKING-SLICE2RTMP/
ADD watch.sh /usr/mcloud/VIKING-SLICE2RTMP/
ENV MILLISECONDSSLICEFLAG 0
COPY --from=builder /root/src/build/SLICE-RTMP /usr/mcloud/VIKING-SLICE2RTMP/SLICE-RTMP
ENTRYPOINT ["/bin/bash","-c","/usr/mcloud/VIKING-SLICE2RTMP/run.sh"] 

