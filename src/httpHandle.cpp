#include "httpHand.h"
#include "Log.h"
// #include "json.h"

HttpHandler::HttpHandler(std::string padPath,std::string pushAddress){
    // m_port = port;
    m_padUrl = padPath;
    m_pushAddress = pushAddress;
    m_isPlay = true;
    m_handleFunc = std::bind(&HttpHandler::handleFunc, this,std::placeholders::_1,std::placeholders::_2);
    m_timeout = 20;
    init();
}
HttpHandler::~HttpHandler(){
    if(m_serverThread.joinable())
        m_serverThread.join();
    if(m_pushPadStream.joinable())
        m_pushPadStream.join();
}

void HttpHandler::init() {
    m_serverThread = std::thread(&HttpHandler::httpHandleThead,std::ref(this->m_handleFunc),&m_operation,&m_padUrl,std::ref(m_isPlay),(void*)&m_mtx);
    pthread_setname_np(m_serverThread.native_handle(),"httpHandleThead");
    m_pushPadStream = std::thread(&HttpHandler::pushPadStream,(void*)this);
    pthread_setname_np(m_pushPadStream.native_handle(),"pushPadStream");
}

std::string HttpHandler::handleFunc(std::string operation,std::string newpadUrl) {
    // std::string opera = std::string((const char*)operation);
    // std::string url = std::string((const char*)padUrl);
    std::string url = "";

    if(operation == "/update"){
        url = newpadUrl;
        SLOG_DEBUG << "operateion = " << "update" << "  " << "padUrl = " << newpadUrl;
    }else if(operation == "/play") {
        url = newpadUrl;
        SLOG_DEBUG << "operateion = " << "play"  << "  " << "padUrl = " << newpadUrl;
    }
    // else if(operation == "/del") {
    //     url = "";
    //     SLOG_DEBUG << "operateion = " << "del"  << "  " << "padUrl = " << newpadUrl;
    // }
    else {
        url = "";
    }

    return url;
}

void HttpHandler::httpHandleThead(std::function<std::string(std::string operation,std::string newpadUrl)> handleFunc,void* operation,void* padUrl,bool& isPlay,void* pMtx) {
    std::mutex* mtx = (std::mutex*)pMtx;
    int port = 8081;
    HttpService router;
    router.POST("/update", [&](const HttpContextPtr& ctx) {
        auto body = ctx->body();
        int index =  body.find(":");
        std::string padPath = body.substr(index+2);
        index = padPath.find_first_of("\"");
        padPath = padPath.substr(0,index);
        
        // Json::Reader reader(Json::Features::strictMode());
        // Json::Value  _root;
        // std::string sBuffer = ctx->body();
        // if (!reader.parse(sBuffer, _root))
        // {
        //     std::string res = "{\r\n    \"state\":\"false\"\r\n}";
        //     return ctx->send(res, ctx->type());
        // }
        // else
        // {
        //     padPath = _root["padPath"].asCString();
        // }

        std::string opera = "/update";
        std::string url = handleFunc(std::string("/update"),padPath);
        memcpy(padUrl,url.c_str(),url.length());
        memcpy(operation,opera.c_str(),opera.length());

        std::string res = "{\r\n    \"state\":\"true\"\r\n}";
        return ctx->send(res, ctx->type());
    });

    router.POST("/play", [&](const HttpContextPtr& ctx) {
        // std::string play = "";
        // Json::Reader reader(Json::Features::strictMode());
        // Json::Value  _root;
        // std::string sBuffer = ctx->body();
        // if (!reader.parse(sBuffer, _root))
        // {
        //     std::string res = "{\r\n    \"state\":\"false\"\r\n}";
        //     return ctx->send(res, ctx->type());;
        // }
        // else
        // {
        //     play = _root["play"].asCString();
        // }


        auto body = ctx->body();
        int index =  body.find_first_of(":");
        std::string play = body.substr(index+2);
        index = play.find_first_of("\"");
        play = play.substr(0,index);

        std::string opera = "/play";
        std::string strPlay = handleFunc("/play",play);
        if(strPlay == "true") {
            isPlay = true;
        }else {
            isPlay = false;
        }

        std::string res = "{\r\n    \"state\":\"true\"\r\n}";
        return ctx->send(res, ctx->type());
    });

    // router.POST("/del", [&](const HttpContextPtr& ctx) {
    //     std::string padPath = "";
    //     Json::Reader reader(Json::Features::strictMode());
    //     Json::Value  _root;
    //     std::string sBuffer = ctx->body();
    //     if (!reader.parse(sBuffer, _root))
    //     {
    //         std::string res = "{\r\n    \"state\":\"false\"\r\n}";
    //         return ctx->send(res, ctx->type());;
    //     }
    //     else
    //     {
    //         padPath = _root["padPath"].asCString();
    //     }

    //     std::string opera = "/del";
    //     std::string url = handleFunc("/del",padPath);
    //     memcpy(padUrl,url.c_str(),url.length());
    //     memcpy(operation,opera.c_str(),opera.length());

    //     std::string res = "{\r\n    \"state\":\"true\"\r\n}";
    //     return ctx->send(res, ctx->type());
    // });

    http_server_t server;
    server.service = &router;
    server.port = port;
#if TEST_HTTPS
    server.https_port = 8443;
    hssl_ctx_init_param_t param;
    memset(&param, 0, sizeof(param));
    param.crt_file = "cert/server.crt";
    param.key_file = "cert/server.key";
    param.endpoint = HSSL_SERVER;
    if (hssl_ctx_init(&param) == NULL) {
        fprintf(stderr, "hssl_ctx_init failed!\n");
        return -20;
    }
#endif

    // uncomment to test multi-processes
    // server.worker_processes = 4;
    // uncomment to test multi-threads
    // server.worker_threads = 4;

    http_server_run(&server, 0);

    // press Enter to stop
    while (getchar() != '\n');
    http_server_stop(&server);

    SLOG_ERROR << "HttpServer exit!!!";
    return;
}

static time_t get_times(){
    struct timeval restrictv;
    gettimeofday(&restrictv,NULL);
	time_t gmt_offset = 0;

    time_t system_gmt = (restrictv.tv_sec + gmt_offset) * 1000
		+ restrictv.tv_usec / 1000;

	return system_gmt;
}

static void ffmpeg_log_callback(void *ptr, int level, const char *fmt, va_list vl)
{
    // if (level > av_log_get_level())
    //     return;
    char temp[1024] = {0};
    std::stringstream ss;
  
    vsnprintf(temp, 1024, fmt, vl);
    ss << temp;
    
    SLOG_ERROR << "ffmpeg: " << ss.str();
    printf("Log:%s\n", ss.str().c_str());
}

void HttpHandler::pushPadStream(void* pHttpHandler) {
    HttpHandler* handler = (HttpHandler*)(pHttpHandler);
    while(1) {
        if(handler->m_isPlay) {
            sleep(1);
            continue;
        }
        
        int re = handler->initInContent();
        if(re < 0) {
            sleep(1);
            continue;
        }
        re = handler->initOutContent();
        if(re < 0) {
            sleep(1);
            continue;
        }
        
        AVPacket pkt;
        AVFormatContext* inctx = handler->m_pInCtx.get();
        AVFormatContext* outctx = handler->m_pOutCtx.get();

        for(;!handler->m_isPlay;) {
            handler->m_lastIntime = get_times();
            re = av_read_frame(inctx,&pkt);
            if (re != 0) {
                break;
            }

            AVRational itime = inctx->streams[pkt.stream_index]->time_base;
            AVRational otime = inctx->streams[pkt.stream_index]->time_base;
            pkt.pts = av_rescale_q_rnd(pkt.pts,itime,otime,
                    (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
            pkt.dts = av_rescale_q_rnd(pkt.dts, itime, otime, 
                    (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
            pkt.duration = av_rescale_q_rnd(pkt.duration, itime, otime, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
            pkt.pos = -1;

            // if(m_pInCtx->streams[pkt.stream_index]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
            //     AVRational tb = m_pInCtx->streams[pkt.stream_index]->time_base;
            //     long long now = av_gettime() - m_startTime;
            //     long long dts = 0;
            //     dts = (pkt.dts - m_firstPktDts) * (1000 * 1000* r2d(tb));
            //     if(dts > now) {
            //         if(dts - now > 1000000)
            //             SLOG_DEBUG << "dts = " << dts << "now = " << now << "sleep = " << dts - now;
            //         av_usleep(dts - now);
            //     }
            // }

            handler->m_lastOuttime = get_times();
            re = av_write_frame(outctx,&pkt);
            if(re < 0) {
                SLOG_ERROR << "Failed to av_write_frame(),re = " << re;
                printf("Failed to av_write_frame(): %ld...\n",re);
                av_packet_unref(&pkt);
                sleep(1);
                continue;
            }

            av_packet_unref(&pkt);
        }
    }
}

std::string HttpHandler::getPadUrl() {
    return m_padUrl;
}

bool HttpHandler::getPlay() {
    return m_isPlay;
}

int interrupt_cb_out(void *ctx)
{
    auto p = (HttpHandler*)ctx;
    time_t end = get_times();
    if((end - p->m_lastOuttime) > p->m_timeout*1000) //10s超时退出    
    {
        SLOG_ERROR << "Failed for timeout!";
        return AVERROR_EOF;
    }
    return 0;
}

int interrupt_cb_in(void *ctx)
{
    auto p = (HttpHandler*)ctx;
    time_t end = get_times();
    if((end - p->m_lastIntime) > p->m_timeout*1000) //10s超时退出    
    {
        SLOG_ERROR << "Failed for timeout!";
        return AVERROR_EOF;
    }
    return 0;
}

int HttpHandler::initOutContent() {
    av_register_all();
    avformat_network_init();

    AVFormatContext *octx = avformat_alloc_context();
    octx->interrupt_callback.opaque = this; //C++
	octx->interrupt_callback.callback = interrupt_cb_out;//设置回调函数，否则有可能ffmpeg一直被挂住。
    int re = avformat_alloc_output_context2(&octx,0,"flv",m_pushAddress.c_str());
    if(!octx) {
        SLOG_ERROR << "[HttpHandler::initOutContent] Failed to avformat_alloc_output_context2(), outUrl = " << m_pushAddress;
        printf("[HttpHandler::initOutContent] Failed to avformat_alloc_output_context2(), outUrl = %s\n",m_pushAddress.c_str());
        avformat_free_context(octx);
        return -1;
    }

    m_pOutCtx = std::shared_ptr<AVFormatContext>(octx,[](AVFormatContext* p){
        avio_close(p->pb);
        avformat_free_context(p);
    });

    //设置timestap允许不连续，解决dts归0不算时间戳跳变
    m_pOutCtx->oformat->flags = AVFMT_TS_DISCONT | AVFMT_TS_NONSTRICT;

    for (int i= 0 ; i < m_pInCtx->nb_streams;i++){
        //create out stream
        AVCodec* codec = avcodec_find_decoder(m_pInCtx->streams[i]->codecpar->codec_id);
        AVStream* out = avformat_new_stream(m_pOutCtx.get(),codec);
        if(out == nullptr) {
            SLOG_ERROR << "[HttpHandler::initOutContent] Failed to avformat_new_stream(),stream index = " << i;
            printf("[HttpHandler::initOutContent] Failed to avformat_new_stream(),stream index=%d\n",i);            
            return -1;
        }

        re = avcodec_parameters_copy(out->codecpar,m_pInCtx->streams[i]->codecpar);
        if(re < 0) {
            SLOG_ERROR << "[HttpHandler::initOutContent] Failed to copy codec parameter,stream index = " << i;
            printf("[HttpHandler::initOutContent] Failed to copy codec parameter,stream index = %d\n",i);
            return -1;
        }
    }

    // push stream
    re = avio_open(&(m_pOutCtx->pb),m_pushAddress.c_str(),AVIO_FLAG_WRITE);
    if(m_pOutCtx->pb == nullptr) {
        SLOG_ERROR << "[HttpHandler::initOutContent] Failed to avio_open(),outUrl = " << m_pushAddress;
        printf("[HttpHandler::initOutContent] Failed to avio_open(),outUrl = %s\n",m_pushAddress.c_str());
        return -1;
    }

    re = avformat_write_header(m_pOutCtx.get(),0);
    if(re < 0) {
        SLOG_ERROR << "[HttpHandler::initOutContent] Failed to avformat_write_header()...";
        printf("[HttpHandler::initOutContent] Failed to avformat_write_header()..\n");
        return -1;
    }

    m_lastOuttime = av_gettime();
    return 0;
}

int HttpHandler::initInContent() {
    av_log_set_level(AV_LOG_INFO);
    av_log_set_callback(ffmpeg_log_callback);
    AVFormatContext *inctx = avformat_alloc_context();
    inctx->interrupt_callback.opaque = this; //C++
	inctx->interrupt_callback.callback = interrupt_cb_in;//设置回调函数，否则有可能ffmpeg一直被挂住。
    
    int re = avformat_open_input(&inctx,m_padUrl.c_str(),0,0);
    m_pInCtx = std::shared_ptr<AVFormatContext>(inctx,[](AVFormatContext* p){
        avformat_free_context(p);
    });

    if(re != 0) {
        SLOG_ERROR << "[HttpHandler::initInContent] Failed to avformat_open_input(), m_padUrl = " << m_padUrl;
        printf("[HttpHandler::initInContent] Failed to avformat_open_input(), m_padUrl = %s\n",m_padUrl.c_str());
        return -1;
    }

    re = avformat_find_stream_info(m_pInCtx.get(), 0);
	if (re != 0)
	{
        SLOG_ERROR << "[HttpHandler::initInContent] Failed to avformat_find_stream_info()...";
        printf("[HttpHandler::initInContent] Failed to avformat_find_stream_info()...\n");
		return -1;
	}

    return 0;
}