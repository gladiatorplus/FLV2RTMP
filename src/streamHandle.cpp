#include "streamHandle.h"
#include "Log.h"
#include <sys/time.h>
#include <sstream>

static void ffmpeg_log_callback(void *ptr, int level, const char *fmt, va_list vl)
{
    if (level > av_log_get_level())
        return;
    char temp[1024] = {0};
    std::stringstream ss;
  
    vsnprintf(temp, 1024, fmt, vl);
    ss << temp;
    
    SLOG_ERROR << "ffmpeg: " << ss.str();
    printf("Log:%s\n", ss.str().c_str());
}

static time_t get_times(){

    struct timeval restrictv;
    gettimeofday(&restrictv,NULL);
	time_t gmt_offset = 0;

    time_t system_gmt = (restrictv.tv_sec + gmt_offset) * 1000
		+ restrictv.tv_usec / 1000;

	return system_gmt;
}

int interrupt_cb1(void *ctx)
{
    auto p = (StreamHandler*)ctx;
    time_t end = get_times();
    if((end - p->m_lasttime) > p->m_timeout*1000) //10s超时退出    
    {
        SLOG_ERROR << "Failed for timeout!";
        return AVERROR_EOF;
    }
    return 0;
}

static double r2d(AVRational r)
{
	return r.num == 0 || r.den == 0 ? 0. : (double)r.num / (double)r.den;
}

StreamHandler::StreamHandler(std::string url) {
    m_outUrl = url;
    m_firstPktDts = -1;
    m_timeout = 20;
}

StreamHandler::~StreamHandler(){
    for(int i =0 ; i < m_qPackets.size() ; i++ ){
        AVPacket* pkt = m_qPackets.front();
        m_qPackets.pop();
        av_packet_free(&pkt);
    }
    for(int i =0 ; i < m_qAPackets.size() ; i++ ){
        AVPacket* pkt = m_qAPackets.front();
        m_qAPackets.pop();
        av_packet_free(&pkt);
    }
}

int StreamHandler::init() {
    av_log_set_level(AV_LOG_INFO);
    av_log_set_callback(ffmpeg_log_callback);
    av_register_all();
    avformat_network_init();

    AVFormatContext *octx = avformat_alloc_context();
    octx->interrupt_callback.opaque = this; //C++
	octx->interrupt_callback.callback = interrupt_cb1;//设置回调函数，否则有可能ffmpeg一直被挂住。
    int re = avformat_alloc_output_context2(&octx,0,"flv",m_outUrl.c_str());
    if(!octx) {
        SLOG_ERROR << "Failed to avformat_alloc_output_context2(), outUrl = " << m_outUrl;
        printf("Failed to avformat_alloc_output_context2(), outUrl = %s\n",m_outUrl.c_str());
        avformat_free_context(octx);
        return -1;
    }

    m_pOutCtx = std::shared_ptr<AVFormatContext>(octx,[](AVFormatContext* p){
        avio_close(p->pb);
        avformat_free_context(p);
    });

    //设置timestap允许不连续，解决dts归0不算时间戳跳变
    m_pOutCtx->oformat->flags = AVFMT_TS_DISCONT | AVFMT_TS_NONSTRICT;

    for (int i= 0 ; i < m_pInCtx->nb_streams;i++){
        //create out stream
        AVCodec* codec = avcodec_find_decoder(m_pInCtx->streams[i]->codecpar->codec_id);
        AVStream* out = avformat_new_stream(m_pOutCtx.get(),codec);
        if(out == nullptr) {
            SLOG_ERROR << "Failed to avformat_new_stream(),stream index = " << i;
            printf("Failed to avformat_new_stream(),stream index=%d\n",i);            
            return -1;
        }

        re = avcodec_parameters_copy(out->codecpar,m_pInCtx->streams[i]->codecpar);
        if(re < 0) {
            SLOG_ERROR << "Failed to copy codec parameter,stream index = " << i;
            printf("Failed to copy codec parameter,stream index = %d\n",i);
            return -1;
        }
    }

    // push stream
    re = avio_open(&(m_pOutCtx->pb),m_outUrl.c_str(),AVIO_FLAG_WRITE);
    if(m_pOutCtx->pb == nullptr) {
        SLOG_ERROR << "Failed to avio_open(),m_outUrl = " << m_outUrl;
        printf("Failed to avio_open(),m_outUrl = %s\n",m_outUrl.c_str());
        return -1;
    }

    re = avformat_write_header(m_pOutCtx.get(),0);
    if(re < 0) {
        SLOG_ERROR << "Failed to avformat_write_header()...";
        printf("Failed to avformat_write_header()..\n");
        return -1;
    }

    m_startTime = av_gettime();
    return 0;
}

void StreamHandler::getPkts(std::string inUrl) {
    AVFormatContext *inctx = avformat_alloc_context();
    m_pInCtx = std::shared_ptr<AVFormatContext>(inctx,[](AVFormatContext* p){
        avformat_free_context(p);
    });

    cur_url = inUrl;
    auto temp = m_pInCtx.get();
    int re = avformat_open_input(&temp,inUrl.c_str(),0,0);
    if(re != 0) {
        SLOG_ERROR << "Failed to avformat_open_input(), inUrl = " << inUrl;
        printf("Failed to avformat_open_input(), inUrl = %s\n",inUrl.c_str());
        return;
    }

    re = avformat_find_stream_info(m_pInCtx.get(), 0);
	if (re != 0)
	{
        SLOG_ERROR << "Failed to avformat_find_stream_info()...";
        printf("Failed to avformat_find_stream_info()...\n");
		return ;
	}

    for(int i =0 ; i < m_pInCtx->nb_streams ; i++){
        if(m_pInCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            m_vIndex = i;
            continue;
        }else if (m_pInCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO){
            m_aIndex = i;
            continue;
        }
    }

    // av_dump_format(m_pInCtx.get(), 0, inUrl.c_str(), 0);

    // num 参数是为了去除.flv文件最后的无用帧的标志
    int num = 0;
    AVPacket pkt;
    SLOG_DEBUG << "Start Read Frame : " << inUrl; 
    
    for(;;) {
        num++;
        re = av_read_frame(m_pInCtx.get(),&pkt);
        if (re != 0) {
            break;
        }
        
        //判断大于最大uint，因在推送时dts=0，但是代码里显示的4294967296，所以直接去掉
        if ((num <=3 || pkt.dts != 0) && pkt.dts <= 4294967295) {
            AVPacket *tmp = av_packet_alloc();
            av_packet_ref(tmp, &pkt);
            if(pkt.stream_index == m_vIndex)
                m_qPackets.push(tmp);
            else if(pkt.stream_index == m_aIndex) {
                m_qAPackets.push(tmp);
            }
        }
            
        if(m_firstPktDts == -1) {
            m_firstPktDts = pkt.dts;
        }

        av_packet_unref(&pkt);
    }

    SLOG_DEBUG << "End Read Frame : " << inUrl; 
}

int StreamHandler::writeFrame() {
    int re = -1;
    AVPacket* tempPkt = m_qPackets.front();
    AVPacket* tempAPkt = m_qAPackets.front();
    AVPacket pktNext = *(m_qPackets.front());
    AVPacket pkt = *(tempPkt);
    AVPacket aPktNext;
    aPktNext.dts = -1;
    AVPacket aPkt;
    aPkt.dts = -1;
    if(0 != m_qAPackets.size()) {
        aPktNext = *(m_qAPackets.front());
        aPkt = *(tempAPkt);
    }

    if(pkt.dts > pktNext.dts || aPkt.dts > aPktNext.dts) { //timestamp not continue
        SLOG_ERROR << "TimeStamp not continue...curDts = " << pkt.dts << " netDts = " << pktNext.dts <<"cur_url"<<cur_url;
        printf("TimeStamp not continue...curDts = %d,netDts = %d\n",pkt.dts,pktNext.dts);
        return re;
    }

    if(aPkt.dts != -1 && pkt.dts > aPkt.dts) {
        pkt = aPkt;
        tempPkt = tempAPkt;
        m_qAPackets.pop();
    }else {
        m_qPackets.pop();
    }
        
    AVRational itime = m_pInCtx->streams[pkt.stream_index]->time_base;
    AVRational otime = m_pOutCtx->streams[pkt.stream_index]->time_base;
    pkt.pts = av_rescale_q_rnd(pkt.pts,itime,otime,
            (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
    pkt.dts = av_rescale_q_rnd(pkt.dts, itime, otime, 
            (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
    pkt.duration = av_rescale_q_rnd(pkt.duration, itime, otime, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
    pkt.pos = -1;

    if(m_pInCtx->streams[pkt.stream_index]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
        AVRational tb = m_pInCtx->streams[pkt.stream_index]->time_base;
        long long now = av_gettime() - m_startTime;
        long long dts = 0;
        dts = (pkt.dts - m_firstPktDts) * (1000 * 1000* r2d(tb));
        if(dts > now) {
            if(dts - now > 1000000)
                SLOG_DEBUG << "dts = " << dts << "now = " << now << "sleep = " << dts - now;
            av_usleep(dts - now);
        }
    }

    m_lasttime = get_times();
    re = av_write_frame(m_pOutCtx.get(),&pkt);
    if(re < 0) {
        SLOG_ERROR << "Failed to av_write_frame(),re = " << re;
        printf("Failed to av_write_frame(): %ld...\n",re);
        av_packet_free(&tempPkt);
        return -1;
    }

    av_packet_free(&tempPkt);
    return 0;
}

int StreamHandler::writeFrameByUrl(std::string inUrl,std::queue<std::string>& qslice,bool& isPlaySlice) {
    AVFormatContext *inctx = avformat_alloc_context();
    m_pInCtx = std::shared_ptr<AVFormatContext>(inctx,[](AVFormatContext* p){
        avformat_free_context(p);
    });

    cur_url = inUrl;
    auto temp = m_pInCtx.get();
    int re = avformat_open_input(&temp,inUrl.c_str(),0,0);
    if(re != 0) {
        SLOG_ERROR << "[StreamHandler::writeFrameByUrl] Failed to avformat_open_input(), inUrl = " << inUrl;
        printf("[StreamHandler::writeFrameByUrl] Failed to avformat_open_input(), inUrl = %s\n",inUrl.c_str());
        return -1;
    }

    re = avformat_find_stream_info(m_pInCtx.get(), 0);
	if (re != 0)
	{
        SLOG_ERROR << "[StreamHandler::writeFrameByUrl] Failed to avformat_find_stream_info()...";
        printf("[StreamHandler::writeFrameByUrl] Failed to avformat_find_stream_info()...\n");
		return -1;
	}

    //outputformat
    init();

    for(int i =0 ; i < m_pInCtx->nb_streams ; i++){
        if(m_pInCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_VIDEO) {
            m_vIndex = i;
            continue;
        }else if (m_pInCtx->streams[i]->codec->codec_type == AVMEDIA_TYPE_AUDIO){
            m_aIndex = i;
            continue;
        }
    }

    // av_dump_format(m_pInCtx.get(), 0, inUrl.c_str(), 0);

    // num 参数是为了去除.flv文件最后的无用帧的标志
    int num = 0;
    AVPacket pkt;
    SLOG_DEBUG << "[StreamHandler::writeFrameByUrl] Start Read Frame : " << inUrl; 
    
    for(;qslice.empty() && isPlaySlice;) {
        num++;
        re = av_read_frame(m_pInCtx.get(),&pkt);
        if (re != 0) {
            break;
        }

        AVRational itime = m_pInCtx->streams[pkt.stream_index]->time_base;
        AVRational otime = m_pOutCtx->streams[pkt.stream_index]->time_base;
        pkt.pts = av_rescale_q_rnd(pkt.pts,itime,otime,
                (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
        pkt.dts = av_rescale_q_rnd(pkt.dts, itime, otime, 
                (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
        pkt.duration = av_rescale_q_rnd(pkt.duration, itime, otime, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_NEAR_INF));
        pkt.pos = -1;

        // if(m_pInCtx->streams[pkt.stream_index]->codecpar->codec_type == AVMEDIA_TYPE_VIDEO) {
        //     AVRational tb = m_pInCtx->streams[pkt.stream_index]->time_base;
        //     long long now = av_gettime() - m_startTime;
        //     long long dts = 0;
        //     dts = (pkt.dts - m_firstPktDts) * (1000 * 1000* r2d(tb));
        //     if(dts > now) {
        //         if(dts - now > 1000000)
        //             SLOG_DEBUG << "dts = " << dts << "now = " << now << "sleep = " << dts - now;
        //         av_usleep(dts - now);
        //     }
        // }

        m_lasttime = get_times();
        re = av_write_frame(m_pOutCtx.get(),&pkt);
        if(re < 0) {
            SLOG_ERROR << "Failed to av_write_frame(),re = " << re;
            printf("Failed to av_write_frame(): %ld...\n",re);
            av_packet_unref(&pkt);
            return -1;
        }

        av_packet_unref(&pkt);
    }
    return 0;
}

int StreamHandler::getPackets() {
    return m_qPackets.size();
}
