

#include "Log.h"

Log::Log(){
}

Log::~Log(){
	if(m_instance != nullptr) {
		delete m_instance;
	}
}

bool Log::Configure(const char* log_dir,const char* log_name,size_t max_file_size,size_t max_files){
	if(m_log)
		return true;
	if(access(log_dir,F_OK) != 0){
		mkdir(log_dir,0755);
	}
	std::string log_file = std::string(log_dir) + log_name;
	m_log = &log4cpp::Category::getInstance("debug");
	log4cpp::Appender * debug_appender = new log4cpp::RollingFileAppender(log_file,log_file,max_file_size,max_files);
	log4cpp::PatternLayout *debug_layout = new log4cpp::PatternLayout();
	debug_layout->setConversionPattern("[%d] %-5p | %m%n");
	debug_appender->setLayout(debug_layout);
	m_log->addAppender(debug_appender);
	m_log->setPriority(log4cpp::Priority::DEBUG);

	return true;
}

log4cpp::Category * Log::Category() {
    return m_log;
}

std::mutex Log::m_mtx;
Log* Log::m_instance = nullptr;

Log * Log::GetInstance(){
	if(m_instance == nullptr) {
		m_mtx.lock();
		if(m_instance == nullptr){
			m_instance = new Log();
		}
		m_mtx.unlock();
	}
	
	return m_instance;
}