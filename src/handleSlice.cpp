#include <unistd.h>
#include "handleSlice.h"
#include "tool.h"
#include "Log.h"
#include <vector>
#include<algorithm>


SliceHandler::SliceHandler(std::string slicePath,time_t& date,std::string& hourSliceDir) {
    m_strPath = slicePath;
    m_strDate = &date;
    m_strHourSliceDir = &hourSliceDir;
    SLOG_DEBUG << "m_strPath= " << m_strPath << " m_strDate= " << *m_strDate << " m_strHourSliceDir= " << *m_strHourSliceDir;
}

SliceHandler::~SliceHandler() {

}

void SliceHandler::sliceProducer(std::queue<std::string>& qSlices){
    DIR              *pDir ; 
    struct dirent    *ent  ; 

    pDir=opendir(m_strPath.c_str()); 
    if(NULL == pDir) {
        SLOG_ERROR << "Open dir Failed : " << m_strPath;
        printf("Open dir Failed : %s\n",m_strPath.c_str());
        closedir(pDir);
        return ;
    }

    std::vector<int> dir_list;
    while((ent=readdir(pDir))!=NULL) 
    { 

        if(ent->d_type & DT_DIR) 
        {   
            if(strcmp(ent->d_name,".")==0 || strcmp(ent->d_name,"..")==0) 
                    continue; 

            std::string subDirName = ent->d_name;
            if(isNum(subDirName)){
                int cur_dir_num =  atoi(subDirName.c_str());
                dir_list.push_back(cur_dir_num);
            }
        } 
    }
    closedir(pDir);

    sort(dir_list.begin(),  dir_list.end());

    for(int i =0;i<dir_list.size();i++){
        std::string subDirName = std::to_string(dir_list[i]);
         if(atoi(m_strHourSliceDir->c_str()) <= dir_list[i]){
            *m_strHourSliceDir = std::to_string(dir_list[i]);
            SLOG_DEBUG << "Open dir : " << m_strPath +subDirName;
            readShiftNsi(m_strPath + subDirName,qSlices);
            SLOG_DEBUG << "Finish dir : " << m_strPath +subDirName;
         }

    }
    return ;
}

int SliceHandler::readShiftNsi(std::string fileName,std::queue<std::string>& qSlice) {
    auto path = fileName + "/shift.nsi";
    std::ifstream f();
    FILE * fp;
	char * line = NULL;
	size_t len = 0;
	ssize_t read;
    SLOG_DEBUG << "start open: " << path;
	fp = fopen(path.c_str(), "r");
    SLOG_DEBUG << "end open: " << path;
    if(nullptr == fp) {
        SLOG_ERROR << "Failed to pen nsi file: " << path;
        printf("Failed to pen nsi file: %s.\n",path.c_str());
        return -1;
    }

    char csvLine[100]={0};//重要！！！ 100 一般设为一行的最大长度
    std::string sliceName;
    while((read = getline(&line, &len, fp)) != -1)
    {
        std::string word(line);
        int index = word.find_first_of('\t');
        if(-1 == index) {
            continue;
        }
        sliceName = word.substr(0,index);
        std::string sliceTime = sliceName.substr(0,sliceName.find_first_of(".flv"));
        if(atol(sliceTime.c_str()) >= *m_strDate) {
            qSlice.push(fileName+ "/" + sliceName);
            *m_strDate = atol(sliceTime.c_str()) + 1; //update slice time 
            continue;
        }else {
            continue;
        }
    }
    SLOG_DEBUG << "start close: " << path;
    fclose(fp);
    SLOG_DEBUG << "end close: " << path;
}
